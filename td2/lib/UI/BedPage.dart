import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BedPage extends StatelessWidget {
  const BedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: Colors.green,
        alignment: Alignment.center,
        child: const Text("Bed"),
      ),
    );
  }
}
