import 'package:flutter/material.dart';
import 'package:td2/theme/mytheme.dart';

import 'UI/MyHomePage.dart';

void main() {
  runApp(MyTd2());
}

class MyTd2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = MyTheme.dark();
    return MaterialApp(
      theme: theme,
      title: 'TD2',
      home: MyHomePage(),
    );
  }
}
