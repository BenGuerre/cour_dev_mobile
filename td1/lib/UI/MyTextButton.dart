import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextButton extends StatelessWidget {
  String myText;
  bool myValue;
  ValueChanged<bool> retrunValue;

  MyTextButton(
      {required this.myText, required this.myValue, required this.retrunValue});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => retrunValue(myValue),
      child: Text(myText, style: const TextStyle(color: Colors.white)),
    );
  }
}
