import 'package:flutter/cupertino.dart';

class IndexChanged extends Notification {
  final int val;
  IndexChanged(this.val);
}
