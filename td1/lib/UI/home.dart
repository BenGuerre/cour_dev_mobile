import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:td1/UI/IndexChanged.dart';

import 'MyIconButton.dart';
import 'MyTextButton.dart';

class MyWidget extends StatefulWidget {
  final Color color;
  final double textsize;
  final String message;

  const MyWidget(this.color, this.textsize, this.message);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  int _index = 0;
  int _score = 0;
  final List _questions = [
    Question.name(
        "The question number 1 is a verylong question and her answer is true.",
        true,
        "flag.png"),
    Question.name("The question number 2 is true again.", true, "flag.png"),
    Question.name("The question number 3 is false.", false, "flag.png"),
    Question.name("The question number 4 is false again.", false, "flag.png"),
    Question.name("The question number 5 is true.", true, "flag.png"),
    Question.name("The question number 6 is true again.", true, "flag.png"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Quizz App"),
          centerTitle: true,
          backgroundColor: Colors.lightBlue,
        ),
        backgroundColor: widget.color,
        body: NotificationListener<IndexChanged>(
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                "images/flag.png",
                width: 250,
                height: 180,
              ),
              Container(
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(105),
                      border: Border.all(
                          color: Color.fromARGB(255, 196, 32, 32),
                          style: BorderStyle.solid)),
                  height: 120.0,
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        _questions[_index].questionText,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          fontSize: widget.textsize,
                        ),
                        textAlign: TextAlign.center,
                      ))),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  MyIconButton(myIcon: Icons.arrow_back, value: -1),
                  MyTextButton(
                      myText: "TRUE", myValue: true, retrunValue: _handleValue),
                  MyTextButton(
                      myText: "FALSE",
                      myValue: false,
                      retrunValue: _handleValue),
                  MyIconButton(myIcon: Icons.arrow_forward, value: 1),
                ],
              )
            ],
          )),
          onNotification: (notification) {
            _changeQuestion(notification.val);
            debugPrint("hey");
            return true;
          },
        ));
  }

  void _handleValue(bool value) {
    debugPrint(value.toString());
    var mySnackBar = SnackBar(
      content: Text(
        value == _questions[_index].isCorrect
            ? "GOOD ANSWER!!"
            : "BAD ANSWER!!",
        style: const TextStyle(fontSize: 20),
        textAlign: TextAlign.center,
      ),
      duration: const Duration(milliseconds: 500),
      backgroundColor: value == _questions[_index].isCorrect
          ? Colors.lightGreen
          : Colors.red,
      width: 180.0, // Width of the SnackBar.
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0, // Inner padding for SnackBar content.
      ),
      behavior: SnackBarBehavior.floating,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(mySnackBar);
    _changeQuestion(1);
    _addScore(value);
  }

  _changeQuestion(int n) {
    if (_index + n < 0 || _index + n >= _questions.length) {
      _toggleScore();
    } else {
      setState(() {
        _index += n;
      });
    }
  }

  void _addScore(bool value) {
    if (value == _questions[_index].isCorrect) {
      setState(() {
        _score++;
      });
    }
  }

  void _toggleScore() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Score"),
            content: Text("Votre score est de $_score / ${_questions.length}"),
            actions: [
              MyTextButton(
                  myText: "OK",
                  myValue: true,
                  retrunValue: (value) {
                    Navigator.of(context).pop();
                    setState(() {
                      _index = 0;
                      _score = 0;
                    });
                  })
            ],
          );
        });
  }
}

class Question {
  final String _questionText;
  final bool _isCorrect;
  final String _image;
  Question.name(this._questionText, this._isCorrect, this._image);
  bool get isCorrect => _isCorrect;
  String get questionText => _questionText;
  String get image => _image;
}
