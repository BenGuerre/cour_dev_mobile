import 'package:flutter/material.dart';
import 'package:td1/UI/home.dart';
import 'UI/home.dart';

void main() {
  runApp(MaterialApp(
      theme: ThemeData(
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blueGrey.shade900,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  )))),
      debugShowCheckedModeBanner: false,
      title: "Application Quizz",
      home: const MyWidget(Colors.teal, 50.00, "Le Quizz ")));
}
